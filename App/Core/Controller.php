<?php
namespace App\Core;

use Twig\Loader\FilesystemLoader;

/**
 * Abstract controller class Controller
 * @package App\Core
 */
abstract class Controller implements ControllerInterface
{
    /** @var View */
    protected $view;
    /** @var FilesystemLoader */
    protected $loader;
    /** @var string */
    protected $title;
    /** @var array */
    protected $data;

    /**
     * Controller constructor
     */
    public function __construct()
    {
        $namespace = array_slice(explode('\\', get_class($this)), -2, 2);
        $viewNamespace = '\App\View';

        foreach ($namespace as $part) {
            $viewNamespace .= "\\" . ucfirst($part);
        }

        $this->view = new $viewNamespace();
    }

    /**
     * Render view
     *
     * @throws \Twig\Error\LoaderError
     * @throws \Twig\Error\RuntimeError
     * @throws \Twig\Error\SyntaxError
     */
    protected function render()
    {
        $this->view->render($this->data);
    }

    /**
     * Set data for view
     *
     * @param $data
     * @return $this
     */
    protected function setData($data)
    {
        $this->data = $data;
        return $this;
    }

    /**
     * Get data for view
     *
     * @return array
     */
    protected function getData()
    {
        return $this->data;
    }
}