<?php

namespace App\Core;

use App\Controller\Index\Index;

/**
 * Routing class Router
 * @package App\Core
 */
class Router
{
    /**
     * Return array of routes
     *
     * @param $action string
     * @return ControllerInterface
     */
    static public function getController($action)
    {
        if ($action == '/') {
            return new Index();
        }

        $actionParts = explode('/', trim($action, '/'), 2);
        $controllerNamespace = '\App\Controller';

        foreach ($actionParts as $part) {
            $controllerNamespace .= "\\" . ucfirst($part);
        }

        if (class_exists($controllerNamespace)) {
            return new $controllerNamespace();
        }

        return null;
    }
}