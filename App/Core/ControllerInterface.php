<?php
namespace App\Core;

use Symfony\Component\HttpFoundation\Request;

/**
 * Interface ControllerInterface
 * @package App\Core
 */
interface ControllerInterface{

    /**
     * Main method of controller
     *
     * @param Request $request
     */
    public function execute($request);
}
