<?php

namespace App\Core;

use Twig\Environment;
use Twig\Loader\FilesystemLoader;

/**
 * Astract view class View
 * @package App\Core
 */
abstract class View
{
    /** @var FilesystemLoader */
    protected $loader;
    /** @var Environment */
    protected $view;
    /** @var string */
    protected $template;
    /** @var string */
    protected $title;

    /**
     * View constructor.
     */
    public function __construct()
    {
        $this->loader = new FilesystemLoader($_SERVER["DOCUMENT_ROOT"] . '/App/templates');
        $this->view = new Environment($this->loader);

        $templateNameArray = array_slice(explode('\\', get_class($this)), -2, 2);
        $templatePath = implode('/', array_map(function ($i) {
            return strtolower($i);
        }, $templateNameArray));

        $this->template = $templatePath . '.html';
    }

    /**
     * Render view
     *
     * @param $data
     * @throws \Twig\Error\LoaderError
     * @throws \Twig\Error\RuntimeError
     * @throws \Twig\Error\SyntaxError
     */
    public function render($data)
    {
        $data['title'] = $this->getTitle();
        echo $this->view->render($this->template, $data);
    }

    /**
     * Set title
     *
     * @param $data
     * @return $this
     */
    public function setTitle($data)
    {
        $this->title = $data;
        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }
}