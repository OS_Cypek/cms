<?php
namespace App;

use App\Controller\Index\Page404;
use App\Core\Controller;
use App\Core\Router;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Yaml\Yaml;

/**
 * Application class App
 * @package App
 */
final class App
{
    /** @var array */
    protected $request;
    /** @var string */
    protected $uri;
    /** @var array */
    protected $namespace;
    /** @var Controller */
    protected $controller;
    /** @var array */
    protected $config;

    /**
     * App constructor
     */
    public function __construct()
    {
        $this->request = Request::createFromGlobals();
        $this->config = Yaml::parseFile($this->request->server->get("DOCUMENT_ROOT") . '/App/Core/config.yml');
        $action = $this->request->getPathInfo();
        $this->namespace = explode('/', trim($action, '/'), 2);
        $this->controller = Router::getController($this->request->getPathInfo());
    }

    /**
     * Run the application
     */
    public function run()
    {
        if (!$this->controller) {
            $this->controller = new Page404();
        }

        $this->controller->execute($this->request);
    }

}
