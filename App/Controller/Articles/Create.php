<?php
namespace App\Controller\Articles;

use App\Core\Controller;

/**
 * Controller lass Index
 * @package App\Controller\Articles
 */
class Create extends Controller
{
    /**
     * @inheritDoc
     */
    public function execute($request)
    {
        $this->setData($request->query->all());
        $this->view->setTitle('Articles Index');
        $this->render();
    }
}