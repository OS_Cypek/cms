<?php
namespace App\Controller\User;

use App\Core\Controller;

/**
 * Controller lass Index
 * @package App\Controller\User
 */
class Index extends Controller
{
    /**
     * @inheritDoc
     */
    public function execute($request)
    {
        $this->setData($request->query->all());
        $this->view->setTitle('User Index');
        $this->render();
    }
}