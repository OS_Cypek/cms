<?php
namespace App\Controller\Index;

use App\Core\Controller;

/**
 * Controller lass Page404
 * @package App\Controller\Index
 */
class Page404 extends Controller
{
    /**
     * @inheritDoc
     */
    public function execute($request)
    {
        $this->setData($request->query->all());
        $this->view->setTitle('404 - Page Not Found');
        $this->render();
    }
}