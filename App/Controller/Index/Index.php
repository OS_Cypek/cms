<?php
namespace App\Controller\Index;

use App\Core\Controller;

/**
 * Controller class Index
 * @package App\Controller\Index
 */
class Index extends Controller
{
    /**
     * @inheritDoc
     */
    public function execute($request)
    {
        $this->setData($request->query->all());
        $this->view->setTitle('Index Index');
        $this->render();
    }
}