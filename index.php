<?php
error_reporting(E_ALL);
ini_set("display_errors", 1);
include_once 'autoload.php';

require __DIR__ . '/vendor/autoload.php';

$app = new \App\App();
$app->run();